<?php

namespace app\models;

use Yii;
use app\models\Lead;
 

/**
 * This is the model class for table "deal".
 *
 * @property integer $Id
 * @property integer $leadId
 * @property string $name
 * @property integer $amount
 *
 * @property Lead $lead
 */
class Deal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'deal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['leadId', 'name', 'amount'], 'required'],
            [['leadId', 'amount'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['leadId'], 'exist', 'skipOnError' => true, 'targetClass' => Lead::className(), 'targetAttribute' => ['leadId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'leadId' => 'Lead ID',
            'name' => 'Name',
            'amount' => 'Amount',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
	//need 
    public function getLeadItem()
    {
        return $this->hasOne(Lead::className(), ['id' => 'leadId']);
    }
	//no need 
	public function getLead() /////////////////////////

   {
	   
	return $this->hasOne(Lead::className(), ['id' => 'leadId']);
    
	}
}
